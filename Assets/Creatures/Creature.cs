﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Creature : MonoBehaviour, ICreatureMovement
{
    public static float SpeedMultiplier = 6000f;
    public static float BoostMultiplier = 300000f;

    public SpriteRenderer spriteRenderer;
    public BoxCollider2D mouseHitbox;
    public BoxCollider2D attackHitbox;
    public BoxCollider2D collisionHitbox;
    public new Rigidbody2D rigidbody;

    public CreatureInfo info;
    public CreatureStats stats;

    public virtual void Boost() {
        if (stats.boosts > 0) {
            stats.boosts--;
            rigidbody.AddForce(transform.up * (stats.weight * BoostMultiplier) * Time.deltaTime);
        }
    }
    
    public virtual void MoveForward() {
        rigidbody.AddForce(transform.up * (stats.speed * SpeedMultiplier) * Time.deltaTime);
    }

    public virtual void LookAt(Vector2 point) {
        transform.up = point;
    }
}
