﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudManager : MonoBehaviour
{
    public Creature creature;
    public Canvas canvas;
    public Text creatureText;
    public Text boostsText;

    void Start()
    {
        creatureText.text = "You are a " + creature.info.name;
    }

    void Update()
    {
        boostsText.text = "You have " + creature.stats.boosts + " boosts (press right mouse to replenish)";
    }
}
