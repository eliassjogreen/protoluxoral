﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : Creature
{
    public static float AirDrag = 0;
    public static float WaterDrag = 5;
    public static float AirGravity = 10;
    public static float WaterGravity = 0;

    public static float LandDrag = 5;

    private bool inWater = false;
    private bool onLand = false;

    private void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.gameObject.tag)
        {
            case "Water":
                Debug.Log("Entered Water");
                inWater = true;
                rigidbody.gravityScale = WaterGravity;
                rigidbody.drag = WaterDrag;
                break;

            case "Land":
                onLand = true;
                rigidbody.gravityScale = AirGravity;
                rigidbody.drag = LandDrag;
                break;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        switch (other.gameObject.tag)
        {
            case "Water":
                Debug.Log("Exited Water");
                inWater = false;
                rigidbody.gravityScale = AirGravity;
                rigidbody.drag = AirDrag;
                break;

            case "Land":
                onLand = false;
                rigidbody.gravityScale = AirGravity;
                rigidbody.drag = AirDrag;
                break;
        }
    }

    public override void Boost() {
        if (stats.boosts > 0) {
            stats.boosts--;
            rigidbody.AddForce(transform.up * (stats.weight * BoostMultiplier) * Time.deltaTime);
        }
    }

    public override void MoveForward() {
        if (inWater || onLand) {
            rigidbody.AddForce(transform.up * (stats.speed * SpeedMultiplier) * Time.deltaTime);
        }
    }

    public override void LookAt(Vector2 point) {
        if (!onLand) {
            transform.up = point;
        }

        if (onLand) {
            float dot = Vector2.Dot(Vector2.left, point);
            if (dot > 0) transform.up = Vector2.left;
            if (dot < 0) transform.up = Vector2.right;
        }
    }
}
