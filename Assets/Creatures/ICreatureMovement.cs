﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICreatureMovement
{
    void Boost();
    void MoveForward();
    void LookAt(Vector2 point);
}