﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Camera camera;
    public Creature creature;
    public string username;

    public uint zoomMin = 10;
    public uint zoomMax = 20;

    [SerializeField]
    private bool boosting = false;

    void Start()
    {
        
    }

    void Update()
    {
        float zoom = Input.GetAxis("Zoom");
        Vector2 mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);

        if (!creature.mouseHitbox.OverlapPoint(mousePosition)) {
            creature.MoveForward();
        }

        if (zoom < 0)
        {
            camera.orthographicSize++;
        }
        if (zoom > 0)
        {
            camera.orthographicSize--;
        }
        camera.orthographicSize = Mathf.Clamp(camera.orthographicSize, zoomMin, zoomMax);


        if (Input.GetAxis("Boost") > 0 && !boosting) {
            creature.Boost();
            boosting = true;
        }
        if (Input.GetAxis("Boost") == 0 && boosting) {
            boosting = false;
        }
        if (Input.GetAxis("Boost") < 0) {
            creature.stats.boosts = 2;
        }

        lookAtMouse();
    }

    void lookAtMouse() {
        Vector2 mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = new Vector2(mousePosition.x - creature.transform.position.x, mousePosition.y - creature.transform.position.y);

        creature.LookAt(direction);
    }
}
