﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Creature Info", menuName = "Creature/Info")]
public class CreatureInfo : ScriptableObject
{
    public new string name;
    public string description;
    
    public Sprite sprite;
}
