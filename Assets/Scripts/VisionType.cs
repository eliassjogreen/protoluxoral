﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VisionType {
    Comun,
    Smell,
    Thermoception,
    Electroreception,
    Echolocation,
    Blindness
}