﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Creature Stats", menuName = "Creature/Stats")]
public class CreatureStats : ScriptableObject
{
    [Range(1, 10)]
    public uint tier;

    // Health
    public uint health;
    [Range(0, 1)]
    public float armor;

    // Damage
    public uint damage;
    [Range(0, 1)]
    public float armorPenetration;

    // Seconds you can spend without <x>
    public uint oxygen;
    public uint pressure;
    public uint stress;

    // Immunities
    [Range(0, 1)]
    public float poisonImmunity;
    [Range(0, 1)]
    public float bleedResistance;
    [Range(0, 1)]
    public float immunity;

    // Visual
    public VisionType vision;
    public bool incognito;

    // Movement
    [Range(0, 2)]
    public float speed;
    public float weight;
    public uint boosts;
    public float boostCooldown;
    public bool chargedBoost;
}
